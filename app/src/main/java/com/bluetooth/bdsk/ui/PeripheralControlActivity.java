package com.bluetooth.bdsk.ui;

import java.util.UUID;
import java.util.UUID.*;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothGattService;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.bluetooth.BluetoothGattCharacteristic;
import com.bluetooth.Constants;
import com.bluetooth.R;
import com.bluetooth.bdsk.bluetooth.BleAdapterService;

import java.util.List;
import java.util.Timer;


public class PeripheralControlActivity extends Activity {
	public static final String EXTRA_NAME = "name";
	public static final String EXTRA_ID = "id";

	private String device_name;
	private String device_address;
	private Timer mTimer;
	private boolean sound_alarm_on_disconnect = false;
	private int led_on;
	private boolean back_requested = false;
	private boolean share_with_server = false;
	private Switch share_switch;
	private BleAdapterService bluetooth_le_adapter;
	private final ServiceConnection service_connection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName componentName, IBinder service) {
			bluetooth_le_adapter = ((BleAdapterService.LocalBinder) service).getService();
			bluetooth_le_adapter.setActivityHandler(message_handler);
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			bluetooth_le_adapter = null;
		}
	};

	@SuppressLint("HandlerLeak")
	private Handler message_handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle;
			String service_uuid = "";
			String characteristic_uuid = "";
			byte[] b = null;
// message handling logic
			switch (msg.what) {
				case BleAdapterService.MESSAGE:
					bundle = msg.getData();
					String text = bundle.getString(BleAdapterService.PARCEL_TEXT);
					showMsg(text);
					break;
				case BleAdapterService.GATT_CONNECTED:
					((Button) PeripheralControlActivity.this
							.findViewById(R.id.connectButton)).setEnabled(false);
// we're connected
					showMsg("CONNECTED");
					// enable the LED buttons
					((Button) PeripheralControlActivity.this.findViewById(R.id.led)).setEnabled(true);
					bluetooth_le_adapter.discoverServices();
					break;
				case BleAdapterService.GATT_DISCONNECT:
					((Button) PeripheralControlActivity.this
							.findViewById(R.id.connectButton)).setEnabled(true);
// we're disconnected
					showMsg("DISCONNECTED");
					// disable led buttons
					((Button)
							PeripheralControlActivity.this.findViewById(R.id.led)).setEnabled(false);
					if (back_requested) {
						PeripheralControlActivity.this.finish();
					}
					break;
				/*case BleAdapterService.GATT_REMOTE_RSSI:
					bundle = msg.getData();
					int rssi = bundle.getInt(BleAdapterService.PARCEL_RSSI);64
					PeripheralControlActivity.this.updateRssi(rssi);*/
					//break;

				case BleAdapterService.GATT_SERVICES_DISCOVERED:
//
					List<BluetoothGattService> slist = bluetooth_le_adapter.getSupportedGattServices();
					boolean SERVICE_LED =false;
					boolean CHARAC_LED =false;
					for (BluetoothGattService svc : slist) {
						Log.d(Constants.TAG, "UUID=" + svc.getUuid().toString().toUpperCase() + " INSTANCE=" + svc.getInstanceId());
						if (svc.getUuid().toString().equalsIgnoreCase(BleAdapterService.service_led)) {
							SERVICE_LED =true;
							for (BluetoothGattCharacteristic character_recv : svc.getCharacteristics()){
								if (character_recv.getUuid().toString().equalsIgnoreCase(BleAdapterService.charac_led)){
									CHARAC_LED=true;
								}
							}
						}
					}
					if (SERVICE_LED&&CHARAC_LED) {
						showMsg("Device has expected services");
// enable the LOW/MID/HIGH alert level selection buttons
						((Button)
								PeripheralControlActivity.this.findViewById(R.id.led)).setEnabled(true);
						((Button)
								PeripheralControlActivity.this.findViewById(R.id.send)).setEnabled(true);
						bluetooth_le_adapter.readCharacteristic(
								BleAdapterService.service_led,BleAdapterService.charac_led);
					} else {
						showMsg("Device does not have expected GATT services");
					}
					break;

				case BleAdapterService.GATT_CHARACTERISTIC_READ:
					bundle = msg.getData();
					Log.d(Constants.TAG, "Service=" +
							bundle.get(BleAdapterService.PARCEL_SERVICE_UUID).toString().toUpperCase() + " Characteristic=" +
							bundle.get(BleAdapterService.PARCEL_CHARACTERISTIC_UUID).toString().toUpperCase());
					if (bundle.get(BleAdapterService.PARCEL_CHARACTERISTIC_UUID).toString()
							.toUpperCase().equals(BleAdapterService.charac_led)
							&& bundle.get(BleAdapterService.PARCEL_SERVICE_UUID).toString()
							.toUpperCase().equals(BleAdapterService.service_led)) {
						b = bundle.getByteArray(BleAdapterService.PARCEL_VALUE);
						if (b.length > 0) {
							PeripheralControlActivity.this.LED_ON((int) b[0]);
						}
					}
					break;
				case BleAdapterService.GATT_CHARACTERISTIC_WRITTEN:
					bundle = msg.getData();
					if (bundle.get(BleAdapterService.PARCEL_CHARACTERISTIC_UUID).toString()
							.toUpperCase().equals(BleAdapterService.charac_led)
							&& bundle.get(BleAdapterService.PARCEL_SERVICE_UUID).toString()
							.toUpperCase().equals(BleAdapterService.service_led)) {
						b = bundle.getByteArray(BleAdapterService.PARCEL_VALUE);
						if (b.length > 0) {
							PeripheralControlActivity.this.LED_ON((int) b[0]);
						}
					}
					break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_peripheral_control);
// read intent data
		final Intent intent = getIntent();
		device_name = intent.getStringExtra(EXTRA_NAME);
		device_address = intent.getStringExtra(EXTRA_ID);
// show the device name
		((TextView) this.findViewById(R.id.nameTextView)).setText("Device : " + device_name + " [" + device_address + "]");

		((Button) this.findViewById(R.id.led)).setEnabled(false);

// connect to the Bluetooth adapter service
		Intent gattServiceIntent = new Intent(this, BleAdapterService.class);
		bindService(gattServiceIntent, service_connection, BIND_AUTO_CREATE);
		showMsg("READY");
	}

	private void showMsg(final String msg) {
		Log.d(Constants.TAG, msg);
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				((TextView) findViewById(R.id.msgTextView)).setText(msg);
			}
		});
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
		unbindService(service_connection);
		bluetooth_le_adapter = null;
	}

	public void onConnect(View view) {
		showMsg("Connecting...!!!");
		if (bluetooth_le_adapter != null) {
			if (bluetooth_le_adapter.connect(device_address)) {
				((Button) PeripheralControlActivity.this
						.findViewById(R.id.connectButton)).setEnabled(false);
			} else {
				showMsg("onConnect: failed to connect");
			}
		} else {
			showMsg("onConnect: bluetooth_le_adapter=null");
		}
	}

	public void onBackPressed() {
		Log.d(Constants.TAG, "onBackPressed");
		back_requested = true;
		if (bluetooth_le_adapter.isConnected()) {
			try {
				bluetooth_le_adapter.disconnect();
			} catch (Exception e) {
			}
		} else {
			finish();
		}
	}

	private void LED_ON(int led_on) {
		this.led_on = led_on;
		((Button) this.findViewById(R.id.led)).setTextColor(Color.parseColor("#000000"));;
		switch (led_on) {
			case 0:
				((Button)
						this.findViewById(R.id.led)).setTextColor(Color.parseColor("#FF0000"));;
				break;
		}
	}

	public void onLed(View view) {
		bluetooth_le_adapter.writeCharacteristic(
				BleAdapterService.service_led,
				BleAdapterService.charac_led, Constants.LED
		);
	}
}