package com.bluetooth;

public class Constants {
	public static final String TAG = "bdsk";
	public static final String FIND = "Find BDSK Devices";
	public static final String STOP_SCANNING = "Stop Scanning";
	public static final String SCANNING = "Scanning";
	public static final byte [] LED = { (byte) 0x01};
}